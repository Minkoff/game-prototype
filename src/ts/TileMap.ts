import TileMapLayer from 'TileMapLayer';
import {
  TiledTileMap, TiledTileMapLayer, TiledTileMapTileSet, TiledTileSet, TiledTileMapTile,
} from 'Tiled';
import TileMapTile from 'TileMapTile';

interface TileSetConfiguration {
  tiledTileSet: TiledTileSet,
  pathFromTileMap: string,
  imageDirectory: string,
}

class TileMap {
  tileWidth: number;

  tileHeight: number;

  tiles: Map<number, TileMapTile> = new Map();

  layers: Map<string, TileMapLayer> = new Map();

  constructor(tiledTileMap: TiledTileMap, ...tileSetConfigurations: Array<TileSetConfiguration>) {
    this.tileWidth = tiledTileMap.tilewidth;
    this.tileHeight = tiledTileMap.tileheight;
    tileSetConfigurations.forEach((tileSetConfiguration: TileSetConfiguration) => {
      const tiledTileMapTileSet: TiledTileMapTileSet = tiledTileMap.tilesets.find(
        (tileSet: TiledTileMapTileSet) => tileSet.source === tileSetConfiguration.pathFromTileMap,
      )!;
      tileSetConfiguration.tiledTileSet.tiles.forEach(
        (tiledTileMapTile: TiledTileMapTile) => {
          this.tiles.set(tiledTileMapTile.id + tiledTileMapTileSet.firstgid,
            new TileMapTile(tiledTileMapTile, tileSetConfiguration.imageDirectory));
        },
      );
    });
    tiledTileMap.layers.forEach((layer: TiledTileMapLayer) => {
      this.layers.set(layer.name, new TileMapLayer(layer, this));
    });
  }

  get tileImageFilenames(): Set<string> {
    const tileImageFilenames: Set<string> = new Set();
    this.layers.forEach((layer: TileMapLayer) => {
      layer.tileImageFilenames.forEach((tileImageFilename: string) => {
        tileImageFilenames.add(tileImageFilename);
      });
    });
    return tileImageFilenames;
  }
}

export default TileMap;
