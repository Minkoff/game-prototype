import Entity from 'Entity';
import GameLoop from 'GameLoop';
import Platform from 'entities/Platform';
import Player from 'entities/Player';
import PlayerSprite from 'sprites/PlayerSprite';
import Scene from 'Scene';
import TileMap from 'TileMap';
import TileMapLayer from 'TileMapLayer';
import { canvas, context } from 'Canvas';
import Position from 'Position';
import { getImageElement } from 'Images';

export default abstract class Level extends Scene {
  cameraPosition: Position;

  foregroundImage: HTMLCanvasElement;

  backgroundImage: HTMLCanvasElement;

  entities: Array<Entity> = [];

  player: Player;

  static backgroundImageFilename: string;

  static tileMap: TileMap;

  static get imageFilenames(): Set<string> {
    const imageFilenames: Set<string> = this.tileMap.tileImageFilenames;
    imageFilenames.add(this.backgroundImageFilename);
    imageFilenames.add(PlayerSprite.imageFilename);
    return imageFilenames;
  }

  constructor(gameLoop: GameLoop) {
    super(gameLoop);
    this.cameraPosition = {
      x: 0,
      y: 0,
    };
    this.player = new Player(this, 500, 500);
    this.entities.push(this.player);
    for (let x = 0; x < this.collisionsLayer.width; x += 1) {
      for (let y = 0; y < this.collisionsLayer.height; y += 1) {
        if (this.collisionsLayer.hasTileAt(x, y)) {
          this.entities.push(new Platform(this,
            x * this.tileMap.tileWidth, y * this.tileMap.tileHeight,
            this.tileMap.tileWidth, this.tileMap.tileHeight));
        }
      }
    }
    this.foregroundImage = this.foregroundLayer.generateImage();
    this.backgroundImage = this.generateBackgroundImage();
  }

  generateBackgroundImage(): HTMLCanvasElement {
    const backgroundImage = global.document.createElement('canvas');
    backgroundImage.width = this.foregroundImage.width;
    backgroundImage.height = this.foregroundImage.height;
    const backgroundImageContext = backgroundImage.getContext('2d')!;
    backgroundImageContext.fillStyle = context.createPattern(getImageElement(this.backgroundImageFilename), 'repeat')!;
    backgroundImageContext.fillRect(0, 0, backgroundImage.width, backgroundImage.height);
    return backgroundImage;
  }

  get tileMap(): TileMap {
    return (<typeof Level> this.constructor).tileMap;
  }

  get backgroundImageFilename(): string {
    return (<typeof Level> this.constructor).backgroundImageFilename;
  }

  get foregroundLayer(): TileMapLayer {
    return this.tileMap.layers.get('foreground')!;
  }

  get collisionsLayer(): TileMapLayer {
    return this.tileMap.layers.get('collisions')!;
  }

  update() {
    const movingEntities = this.entities.filter((entity) => !entity.isStationary);
    movingEntities.forEach((entity) => entity.update());
    movingEntities.forEach((entity) => entity.updateVerticalPosition());
    movingEntities.forEach((entity) => entity.handleVerticalCollisions());
    movingEntities.forEach((entity) => entity.updateHorizontalPosition());
    movingEntities.forEach((entity) => entity.handleHorizontalCollisions());
  }

  setFocalPoint(focalPoint: Position): void {
    this.cameraPosition.x = focalPoint.x - (canvas.width / 2);
    this.cameraPosition.y = focalPoint.y - (canvas.height / 2);
  }

  hasInView(x: number, y: number, width: number, height: number): boolean {
    return x < this.cameraPosition.x + canvas.width && x + width > this.cameraPosition.x
      && y < this.cameraPosition.y + canvas.height && y + height > this.cameraPosition.y;
  }

  drawLayerImage(image: HTMLCanvasElement) {
    context.drawImage(image,
      Math.round(this.cameraPosition.x), Math.round(this.cameraPosition.y),
      canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);
  }

  render(timeOffset: number) {
    this.setFocalPoint({
      x: this.player.x + (this.player.width / 2),
      y: this.player.y + (this.player.height / 2),
    });
    context.clearRect(0, 0, canvas.width, canvas.height);
    this.drawLayerImage(this.backgroundImage);
    this.drawLayerImage(this.foregroundImage);
    this.entities.forEach((entity) => {
      if (this.hasInView(entity.x, entity.y, entity.width, entity.height)) {
        entity.render(timeOffset);
      }
    });
  }
}
