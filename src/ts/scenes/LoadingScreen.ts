import Scene from 'Scene';
import DemoLevel from 'scenes/DemoLevel';
import GameLoop from 'GameLoop';
import { loadImages } from 'Images';

export default class LoadingScreen extends Scene {
  constructor(gameLoop: GameLoop) {
    super(gameLoop);

    // Load the images used in the level. When the images have finished loading...
    loadImages(DemoLevel.imageFilenames).then(() => {
      // Change the scene of the game loop to the level.
      this.gameLoop.scene = new DemoLevel(gameLoop);
    });
  }

  update(): void {
  }

  render(): void {
  }
}
