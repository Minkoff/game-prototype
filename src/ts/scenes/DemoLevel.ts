import Level from 'scenes/Level';
import { TiledTileMap, TiledTileSet } from 'Tiled';
import TileMap from 'TileMap';

const tiledTileMap : TiledTileMap = require('../../assets/tilemaps/tilemap.json');
const tilesetTileset: TiledTileSet = require('../../assets/tilesets/tileset/tileset.json');
const collisionTileset: TiledTileSet = require('../../assets/tilesets/collisions/collisions.json');

export default class DemoLevel extends Level {
  static backgroundImageFilename: string = './patterns/sky.jpg';

  static tileMap: TileMap = new TileMap(tiledTileMap,
    {
      tiledTileSet: tilesetTileset,
      pathFromTileMap: '../tilesets/tileset/tileset.json',
      imageDirectory: './tilesets/tileset',
    },
    {
      tiledTileSet: collisionTileset,
      pathFromTileMap: '../tilesets/collisions/collisions.json',
      imageDirectory: './tilesets/collisions',
    });
}
