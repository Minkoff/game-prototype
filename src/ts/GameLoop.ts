import LoadingScreen from 'scenes/LoadingScreen';
import Scene from 'Scene';

export const tickDuration: number = Math.round(1000 / 60);
export default class GameLoop {
  accumulator: number;

  previousTime: number;

  scene: Scene;

  constructor() {
    this.accumulator = 0;
    this.previousTime = 0;
    this.scene = new LoadingScreen(this);
  }

  start() {
    global.requestAnimationFrame(this.frameRequestCallback.bind(this));
  }

  frameRequestCallback(currentTime: DOMHighResTimeStamp): void {
    this.accumulator += currentTime - this.previousTime;
    this.previousTime = currentTime;
    while (this.accumulator >= tickDuration) {
      this.scene.update();
      this.accumulator -= tickDuration;
    }
    this.scene.render(this.accumulator);
    global.requestAnimationFrame(this.frameRequestCallback.bind(this));
  }
}
