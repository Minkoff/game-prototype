/** The canvas element that the game is rendered in. */
export const canvas: HTMLCanvasElement = global.document.getElementById('scene') as HTMLCanvasElement;

/** The 2D rendering context of the canvas element that the game is rendered in. */
export const context: CanvasRenderingContext2D = canvas.getContext('2d') as CanvasRenderingContext2D;

context.imageSmoothingEnabled = false;
