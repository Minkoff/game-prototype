export interface TiledTileMapTile {
  id: number,
  image: string,
  imageheight: number,
  imagewidth: number
}

interface TiledTileSetGrid {
  height: number,
  orientation: string,
  width: number
}

export interface TiledTileSet {
  columns: number,
  grid: TiledTileSetGrid,
  margin: number,
  name: string,
  spacing: number,
  tilecount: number,
  tiledversion: string,
  tileheight: number,
  tiles: Array<TiledTileMapTile>,
  tilewidth: number,
  type: string,
  version: number
}

export interface TiledTileMapLayer {
  data: Array<number>,
  height: number,
  id: number,
  name: string,
  opacity: number,
  type: string,
  visible: boolean,
  width: number,
  x: number,
  y: number
}

export interface TiledTileMapTileSet {
  firstgid: number,
  source: string
}

export interface TiledTileMap {
  height: number,
  infinite: boolean,
  layers: Array<TiledTileMapLayer>,
  nextobjectid: number,
  orientation: string,
  renderorder: string,
  tiledversion: string,
  tileheight: number,
  tilesets: Array<TiledTileMapTileSet>,
  tilewidth: number,
  type: string,
  version: number,
  width: number
}
