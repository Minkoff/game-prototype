import GameLoop from 'GameLoop';
import { canvas } from 'Canvas';
import { listenForInput } from 'Input';

const minWidth = 640;
const minHeight = 480;
const maxWidth = 1280;
const maxHeight = 720;

const updateGameContainerDimensions = () => {
  const width = (document.fullscreenElement)
    ? global.screen.width
    : global.window.innerWidth;
  const height = (document.fullscreenElement)
    ? global.screen.height
    : global.window.innerHeight;
  canvas.width = Math.max(Math.min(width, maxWidth), minWidth);
  canvas.height = Math.max(Math.min(height, maxHeight), minHeight);
};
global.addEventListener('resize', () => updateGameContainerDimensions());
updateGameContainerDimensions();

// Listen for keyboard and controller input.
listenForInput();

// Instantiate the game loop.
const gameLoop = new GameLoop();

// Start the game loop.
gameLoop.start();

const requestFullscreen = () => {
  if (canvas.requestFullscreen) {
    canvas.requestFullscreen();
  } else {
    /* @ts-ignore */
    canvas.webkitRequestFullscreen();
  }
};

global.document.querySelector('#fullscreen')!.addEventListener('click', requestFullscreen);
