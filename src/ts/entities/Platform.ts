import Entity from 'Entity';

export default class Platform extends Entity {
  isStationary: boolean = true;

  isPlatform: boolean = true;
}
