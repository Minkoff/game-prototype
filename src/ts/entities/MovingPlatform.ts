import Entity from 'Entity';
import Level from 'scenes/Level';
import Platform from 'entities/Platform';
import { context } from 'Canvas';

class MovingPlatform extends Platform {
  isStationary: boolean = false;

  startX: number;

  startY: number;

  endX: number;

  endY: number;

  color: string = '#7ebf32';

  constructor(level: Level, width: number, height: number, startX: number, startY: number,
    endX: number, endY: number, velocity: number) {
    super(level, startX, startY, width, height);

    const distanceX = Math.abs(endX - startX);
    const distanceY = Math.abs(endY - startY);
    const distance = Math.sqrt(distanceX ** 2 + distanceY ** 2);

    this.velocityX = (distanceX / distance) * velocity;
    this.velocityY = (distanceY / distance) * velocity;
    this.startX = startX;
    this.startY = startY;
    this.endX = endX;
    this.endY = endY;
  }

  update(): void {
    super.update();
    if (this.x > this.endX || this.y > this.endY
    || this.x < this.startX || this.y < this.startY) {
      this.velocityX = -this.velocityX;
      this.velocityY = -this.velocityY;
    }
  }

  handleCollisionWithPlatform(entity: Entity): void {
    if (this.willCollideFromTop(entity) || this.willCollideFromBottom(entity)
    || this.willCollideFromLeft(entity) || this.willCollideFromRight(entity)) {
      this.velocityY = -this.velocityY;
      this.velocityX = -this.velocityX;
    }
  }

  render(): void {
    context.fillStyle = this.color;
    context.fillRect(this.x, this.y, this.width, this.height);
  }
}

export default MovingPlatform;
