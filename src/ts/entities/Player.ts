import Entity from 'Entity';
import PlayerSprite from 'sprites/PlayerSprite';
import Level from 'scenes/Level';
import Direction from 'Direction';
import { tickDuration } from 'GameLoop';
import {
  KeyCode, GamepadAxis, GamepadButton, keyDown, buttonDown, axisValue,
} from 'Input';

class Player extends Entity {
  playerWidth: number = 26;

  standingHeight: number = 63;

  jumpingHeight: number = 66;

  fallingHeight: number = 67;

  crouchingHeight: number = 48;

  runAcceleration: number = 0.002;

  jumpVelocity: number = 0.5;

  jumpDisabled: boolean = false;

  jetpackVelocity: number = 0.25;

  jetpackMaxFuel: number = 500;

  jetpackFuel: number = 500;

  jetpackDisabled: boolean = false;

  currentDirection: number = Direction.Right;

  distanceTravelledSinceStopped: number = 0;

  sprite: PlayerSprite;

  hitboxVisible: boolean = false;

  constructor(level: Level, x: number, y: number) {
    super(level, x, y, 26, 63);

    this.terminalVelocityX = 0.5;
    this.terminalVelocityY = 2;
    this.gravity = 0.001;
    this.friction = 0.001;

    this.sprite = new PlayerSprite(this);
  }

  get isCrouching(): boolean {
    return this.isGrounded && this.isPressingCrouchButton;
  }

  get isRunning(): boolean {
    return this.isGrounded && !this.isCrouching && Math.abs(this.velocityX) > 0;
  }

  get isStanding(): boolean {
    return this.isGrounded && !this.isRunning && !this.isCrouching;
  }

  get isJumping(): boolean {
    return !this.isGrounded && this.velocityY < 0;
  }

  get isFalling(): boolean {
    return !this.isGrounded && !this.isJumping;
  }

  get isFacingLeft(): boolean {
    return this.currentDirection === Direction.Left;
  }

  get isUsingJetpack(): boolean {
    return this.jetpackFuel > 0 && !this.jetpackDisabled && this.isPressingJumpButton;
  }

  get isPressingJumpButton(): boolean {
    return keyDown(KeyCode.Space) || buttonDown(GamepadButton.Jump);
  }

  get isPressingCrouchButton(): boolean {
    return keyDown(KeyCode.ArrowDown) || axisValue(GamepadAxis.Vertical) > 0.5;
  }

  update(): void {
    super.update();
    // Track distance travelled.
    const distanceTravelled = Math.abs(this.x - this.prevX);
    if (distanceTravelled !== 0) {
      this.distanceTravelledSinceStopped += distanceTravelled;
    } else {
      this.distanceTravelledSinceStopped = 0;
    }

    // Refuel jetpack if grounded.
    if (this.isGrounded) {
      this.jetpackFuel = this.jetpackMaxFuel;
    }

    // Handle horizontal movement.
    if (this.isCrouching || (keyDown(KeyCode.ArrowLeft) && keyDown(KeyCode.ArrowRight))) {
      this.accelerationX = 0;
    } else if (keyDown(KeyCode.ArrowLeft)) {
      this.accelerationX = -this.runAcceleration;
      this.currentDirection = Direction.Left;
    } else if (keyDown(KeyCode.ArrowRight)) {
      this.accelerationX = this.runAcceleration;
      this.currentDirection = Direction.Right;
    } else if (axisValue(GamepadAxis.Horizontal) < -0.01) {
      this.accelerationX = this.runAcceleration * axisValue(GamepadAxis.Horizontal);
      this.currentDirection = Direction.Left;
    } else if (axisValue(GamepadAxis.Horizontal) > 0.01) {
      this.accelerationX = this.runAcceleration * axisValue(GamepadAxis.Horizontal);
      this.currentDirection = Direction.Right;
    } else {
      this.accelerationX = 0;
    }

    // Handle vertical movement.
    if (this.isPressingJumpButton && !this.isCrouching) {
      if (this.isGrounded && !this.jumpDisabled) {
        this.velocityY = -this.jumpVelocity;
        this.jetpackDisabled = true;
        this.jumpDisabled = true;
      } else if (!this.jetpackDisabled && this.jetpackFuel > 0) {
        this.jetpackFuel -= tickDuration;
        this.velocityY = Math.min(this.velocityY, this.jetpackVelocity * -1);
      }
    } else if (this.isGrounded) {
      this.jumpDisabled = false;
    } else {
      this.jetpackDisabled = false;
    }

    this.height = this.standingHeight;
    if (this.isJumping) {
      this.height = this.jumpingHeight;
    } else if (this.isFalling) {
      this.height = this.fallingHeight;
    } else if (this.isCrouching) {
      this.height = this.crouchingHeight;
    }
    this.y -= (this.height - this.prevHeight);
  }

  render(timeOffset: number) {
    // Render the Sprite. TODO: Consider taking timeOffset into account.
    this.sprite.render();
    super.render(timeOffset);
  }
}

export default Player;
