import { tickDuration } from 'GameLoop';
import Level from 'scenes/Level';
import { context } from 'Canvas';

export default abstract class Entity {
  level: Level;

  x: number;

  y: number;

  width: number;

  height: number;

  prevX: number;

  prevY: number;

  prevWidth: number;

  prevHeight: number;

  isStationary: boolean = false;

  isPlatform: boolean = false;

  velocityX: number;

  velocityY: number;

  accelerationX: number;

  accelerationY: number;

  terminalVelocityX: number;

  terminalVelocityY: number;

  gravity: number;

  friction: number;

  floor: Entity|null;

  hitboxVisible: boolean = false;

  hitboxColor: string = 'rgba(255, 0, 0, 0.5)';

  constructor(level: Level, x: number, y: number, width: number, height: number) {
    this.level = level;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.prevX = x;
    this.prevY = y;
    this.prevWidth = 0;
    this.prevHeight = 0;
    this.velocityX = 0;
    this.velocityY = 0;
    this.accelerationX = 0;
    this.accelerationY = 0;
    this.terminalVelocityX = 10000;
    this.terminalVelocityY = 10000;
    this.gravity = 0;
    this.friction = 0;
    this.floor = null;
  }

  get isGrounded(): boolean {
    return this.floor !== null;
  }

  update(): void {
    this.prevWidth = this.width;
    this.prevHeight = this.height;
  }

  render(timeOffset: number): void {
    // Draw the hitbox.
    if (this.hitboxVisible) {
      context.fillStyle = this.hitboxColor;
      context.fillRect(this.x - this.level.cameraPosition.x, this.y - this.level.cameraPosition.y,
        this.width, this.height);
    }
  }

  updateVerticalPosition(): void {
    this.prevY = this.y;

    // Apply acceleration and gravity to velocity
    this.velocityY += this.accelerationY * tickDuration;
    this.velocityY += this.gravity * tickDuration;

    // Limit velocity
    this.velocityY = this.velocityY >= 0
      ? Math.min(this.velocityY, this.terminalVelocityY)
      : Math.max(this.velocityY, -this.terminalVelocityY);

    // Account for the velocity of the floor
    const velocityY = (this.floor !== null)
      ? this.velocityY + this.floor.velocityY
      : this.velocityY;

    // Apply velocity to position
    this.y = this.prevY + (velocityY * tickDuration);
  }

  updateHorizontalPosition(): void {
    this.prevX = this.x;

    // Apply acceleration to velocity
    this.velocityX += this.accelerationX * tickDuration;

    // Apply friction to velocity
    if (this.velocityX > 0) {
      this.velocityX = Math.max(this.velocityX - (this.friction * tickDuration), 0);
    } else if (this.velocityX < 0) {
      this.velocityX = Math.min(this.velocityX + (this.friction * tickDuration), 0);
    }

    // Limit velocity
    this.velocityX = this.velocityX >= 0
      ? Math.min(this.velocityX, this.terminalVelocityX)
      : Math.max(this.velocityX, -this.terminalVelocityX);

    // Account for the velocity of the floor
    const velocityX = (this.floor !== null)
      ? this.velocityX + this.floor.velocityX
      : this.velocityX;

    // Apply velocity to position
    this.x = this.prevX + (velocityX * tickDuration);
  }

  handleVerticalCollisions(): void {
    this.floor = null;
    this.level.entities.forEach((entity: Entity) => {
      if (entity.isPlatform) {
        if (this.willCollideFromTop(entity)) {
          this.velocityY = this.gravity;
          this.y = entity.y - this.height;
          this.floor = entity;
        } else if (this.willCollideFromBottom(entity)) {
          this.velocityY = 0;
          this.y = entity.y + entity.height;
        }
      }
    });
  }

  handleHorizontalCollisions(): void {
    this.level.entities.forEach((entity: Entity) => {
      if (entity.isPlatform) {
        if (this.willCollideFromLeft(entity)) {
          this.velocityX = 0;
          this.x = entity.x - this.width;
        } else if (this.willCollideFromRight(entity)) {
          this.velocityX = 0;
          this.x = entity.x + entity.width;
        }
      }
    });
  }

  willCollideFromTop(entity: Entity): boolean {
    return this.willIntersectHorizontally(entity) && this.prevY + this.height <= entity.prevY
      && this.y + this.height >= entity.y;
  }

  willCollideFromBottom(entity: Entity): boolean {
    return this.willIntersectHorizontally(entity) && this.prevY >= entity.prevY + entity.height
      && this.y <= entity.y + entity.height;
  }

  willCollideFromLeft(entity: Entity): boolean {
    return this.willIntersectVertically(entity) && this.prevX + this.width <= entity.prevX
      && this.x + this.width >= entity.x;
  }

  willCollideFromRight(entity: Entity): boolean {
    return this.willIntersectVertically(entity) && this.prevX >= entity.prevX + entity.width
      && this.x <= entity.x + entity.width;
  }

  willIntersectHorizontally(entity: Entity): boolean {
    return this.x < entity.x + entity.width && this.x + this.width > entity.x;
  }

  willIntersectVertically(entity: Entity): boolean {
    return this.y < entity.y + entity.height && this.y + this.height > entity.y;
  }
}
