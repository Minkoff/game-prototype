import GameLoop from 'GameLoop';

export default abstract class Scene {
  gameLoop: GameLoop;

  constructor(gameLoop: GameLoop) {
    this.gameLoop = gameLoop;
  }

  update(): void {
  }

  render(timeOffset: number): void {
  }
}
