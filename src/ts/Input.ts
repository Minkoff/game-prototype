/** The KeyboardEvent key codes of the keyboard keys that are currently being pressed. */
const downKeys: Set<string> = new Set();

/** The Gamepads that are currently connected. */
const connectedGamepads: Map<number, Gamepad> = new Map();

/** A key code returned by a KeyboardEvent. */
export enum KeyCode {
  Space = 'Space',
  ArrowLeft = 'ArrowLeft',
  ArrowRight = 'ArrowRight',
  ArrowUp = 'ArrowUp',
  ArrowDown = 'ArrowDown',
  Enter = 'Enter',
  Escape = 'Escape',
  Tab = 'Tab'
}

/** An axis of a Gamepad. */
export enum GamepadAxis {
  Horizontal,
  Vertical
}

/** A button of a Gamepad. */
export enum GamepadButton {
  Jump = 0
}

/**
 * Add a Gamepad to the set of connected Gamepads.
 *
 * @param gamepad - The Gamepad to add to the set of connected Gamepads.
 */
function addGamepad(gamepad: Gamepad): void {
  connectedGamepads.set(gamepad.index, gamepad);
}

/**
 * Remove a Gamepad from the set of connected Gamepads.
 *
 * @param gamepad - The Gamepad to remove from the set of connected Gamepads.
 */
function removeGamepad(gamepad: Gamepad): void {
  connectedGamepads.delete(gamepad.index);
}

/**
 * Get the Gamepads that are currently connected and add them to the map of connected Gamepads.
 */
function scanGamepads(): void {
  // Get all of the Gamepads that are currently connected.
  const gamepads: Array<Gamepad|null> = global.navigator.getGamepads();
  // Add each non-null Gamepad to the map of connected Gamepads.
  (Array.from(gamepads).filter((gamepad: Gamepad|null) => gamepad !== null) as Array<Gamepad>)
    .forEach((gamepad: Gamepad) => {
      addGamepad(gamepad);
    });
}

/**
 * Listen for global input events and update the state of the Input module accordingly.
 */
export function listenForInput(): void {
  // When a `keydown` event occurs, add the key code of the event to the set of keys that are down.
  global.addEventListener('keydown', (e: KeyboardEvent) => {
    downKeys.add(e.code);
  });
  // When a `keyup` event occurs,
  // remove the key code of the event from the set of keys that are down.
  global.addEventListener('keyup', (e: KeyboardEvent) => {
    downKeys.delete(e.code);
  });
  // When a `gamepadconnected` event occurs,
  // add the Gamepad of the event to the list of connected Gamepads.
  // @ts-ignore: https://github.com/microsoft/TypeScript/issues/39425
  global.addEventListener('gamepadconnected', (e: GamepadEvent) => {
    addGamepad(e.gamepad);
  });
  // When a `gamepadconnected` event occurs,
  // remove the Gamepad of the event from the list of connected Gamepads.
  // @ts-ignore: https://github.com/microsoft/TypeScript/issues/39425
  global.addEventListener('gamepaddisconnected', (e: GamepadEvent) => {
    removeGamepad(e.gamepad);
  });
}

/**
 * Determine if the key with the provided key code is currently down.
 *
 * @param keyCode - The key code of the key.
 * @returns Whether the key is currently down.
 */
export function keyDown(keyCode: KeyCode): boolean {
  return downKeys.has(keyCode);
}

/**
 * Determine if the provided Gamepad button is currently down.
 *
 * @param gamepadButton - The Gamepad button.
 * @returns Whether the Gamepad button is currently down.
 */
export function buttonDown(gamepadButton: GamepadButton): boolean {
  return Object.values(connectedGamepads).some(
    (gamepad: Gamepad) => gamepad.buttons[gamepadButton] !== undefined
      && gamepad.buttons[gamepadButton].pressed,
  );
}

/**
 * Determine the value of the provided Gamepad axis.
 *
 * @param gamepadButton - The Gamepad axis.
 * @returns The value of the Gamepad axis.
 */
export function axisValue(gamepadAxis: GamepadAxis): number {
  // Try to find a connected Gamepad that has the provided axis.
  const gamepadWithAxis: Gamepad|null = Object.values(connectedGamepads).find(
    (gamepad: Gamepad) => gamepad.axes[gamepadAxis] !== undefined,
  );
  // If a gamepad with the axis was found, return the value of the axis. Otherwise, return 0.
  return gamepadWithAxis ? gamepadWithAxis.axes[gamepadAxis] : 0;
}

scanGamepads();
