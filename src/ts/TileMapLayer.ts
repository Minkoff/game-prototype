import TileMap from 'TileMap';
import TileMapTile from 'TileMapTile';
import { getImageElement } from 'Images';
import { TiledTileMapLayer } from 'Tiled';

class TileMapLayer {
  name: string;

  width: number;

  height: number;

  tileIDs: Array<number>;

  tileMap: TileMap;

  constructor(tiledTileMapLayer: TiledTileMapLayer, tileMap: TileMap) {
    this.name = tiledTileMapLayer.name;
    this.width = tiledTileMapLayer.width;
    this.height = tiledTileMapLayer.height;
    this.tileIDs = tiledTileMapLayer.data;
    this.tileMap = tileMap;
  }

  get tileImageFilenames(): Set<string> {
    const tileImageFilenames: Set<string> = new Set();
    for (let x = 0; x < this.width; x += 1) {
      for (let y = 0; y < this.height; y += 1) {
        if (this.hasTileAt(x, y)) {
          tileImageFilenames.add(this.getTileAt(x, y).imageFilename);
        }
      }
    }
    return tileImageFilenames;
  }

  hasTileAt(x: number, y: number): boolean {
    return this.tileIDs[y * this.width + x] !== 0;
  }

  getTileAt(x: number, y: number): TileMapTile {
    const tileID = this.tileIDs[y * this.width + x];
    return this.tileMap.tiles.get(tileID)!;
  }

  generateImage(): HTMLCanvasElement {
    // Create an off-screen canvas.
    const canvas = global.document.createElement('canvas');
    canvas.width = this.width * this.tileMap.tileWidth;
    canvas.height = this.height * this.tileMap.tileHeight;

    // Get the rendering context.
    const context = canvas.getContext('2d')!;

    // Iterate over X, Y coordinates.
    for (let x = 0; x < this.width; x += 1) {
      for (let y = 0; y < this.height; y += 1) {
        // If the tile was found, render it to the off-screen canvas.
        if (this.hasTileAt(x, y)) {
          const tile = this.getTileAt(x, y);
          context.drawImage(getImageElement(tile.imageFilename), 0, 0,
            tile.imageWidth, tile.imageHeight,
            x * this.tileMap.tileWidth, y * this.tileMap.tileHeight,
            tile.imageWidth, tile.imageHeight);
        }
      }
    }

    // Return the off-screen canvas.
    return canvas;
  }
}

export default TileMapLayer;
