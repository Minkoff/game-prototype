import { TiledTileMapTile } from 'Tiled';

export default class TileMapTile {
  id: number;

  imageFilename: string;

  imageWidth: number;

  imageHeight: number;

  constructor(tiledTileMapTile: TiledTileMapTile, imageDirectory: string) {
    this.id = tiledTileMapTile.id;
    this.imageFilename = `${imageDirectory}/${tiledTileMapTile.image}`;
    this.imageWidth = tiledTileMapTile.imagewidth;
    this.imageHeight = tiledTileMapTile.imageheight;
  }
}
