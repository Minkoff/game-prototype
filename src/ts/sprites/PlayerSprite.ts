import Sprite from 'Sprite';
import Player from 'entities/Player';

class PlayerSprite extends Sprite {
  player: Player;

  static imageFilename: string = './sprites/robot.png';

  constructor(player: Player) {
    super(player, PlayerSprite.imageFilename, 96, 96);
    this.player = player;
  }

  get offsetX(): number {
    return -36;
  }

  get offsetY(): number {
    if (this.player.isGrounded) {
      if (!this.player.isRunning) {
        if (!this.player.isCrouching) {
          return -17;
        }
        return -32;
      }
      return -17;
    }
    if (!this.player.isUsingJetpack) {
      if (this.player.velocityY < 0) {
        return -10;
      }
      return -11;
    }
    return -14;
  }

  get x(): number {
    return this.player.x + this.offsetX;
  }

  get y(): number {
    return this.player.y + this.offsetY;
  }

  get currentSheet(): number {
    if (this.player.isGrounded) {
      if (!this.player.isRunning) {
        if (!this.player.isCrouching) {
          return this.player.isFacingLeft ? 0 : 1;
        }
        return this.player.isFacingLeft ? 2 : 3;
      }
      return this.player.isFacingLeft ? 4 : 5;
    }
    if (!this.player.isUsingJetpack) {
      if (this.player.velocityY < 0) {
        return this.player.isFacingLeft ? 6 : 7;
      }
      return this.player.isFacingLeft ? 8 : 9;
    }
    return this.player.isFacingLeft ? 10 : 11;
  }

  get currentFrame(): number {
    if (this.player.isGrounded) {
      if (!this.player.isRunning) {
        return 0;
      }
      return Math.round(Math.abs(this.player.distanceTravelledSinceStopped) / 40) % 6;
    }
    if (!this.player.isUsingJetpack) {
      return 0;
    }
    return Math.round(Math.abs(this.player.jetpackFuel) / 40) % 8;
  }
}

export default PlayerSprite;
