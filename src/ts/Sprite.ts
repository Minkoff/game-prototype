import { context } from 'Canvas';
import { getImageElement } from 'Images';
import Entity from 'Entity';

class Sprite {
  entity: Entity;

  imageFilename: string;

  width: number;

  height: number;

  constructor(entity: Entity, imageFilename: string, width: number, height: number) {
    this.entity = entity;
    this.imageFilename = imageFilename;
    this.width = width;
    this.height = height;
  }

  get x() {
    return this.entity.x;
  }

  get y() {
    return this.entity.y;
  }

  get currentSheet() {
    return 0;
  }

  get currentFrame() {
    return 0;
  }

  render() {
    context.drawImage(getImageElement(this.imageFilename),
      this.currentFrame * this.width, this.currentSheet * this.height,
      this.width, this.height,
      Math.round(this.x - this.entity.level.cameraPosition.x),
      Math.round(this.y - this.entity.level.cameraPosition.y),
      this.width, this.height);
  }
}

export default Sprite;
