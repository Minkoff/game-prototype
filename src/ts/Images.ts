/** A map of image file paths (relative to the `assets` directory) to their corresponding URLs. */
const imageURLs: Map<string, string> = new Map();

/** A map of image filenames to `HTMLImageElement`s. */
const imageElements: Map<string, HTMLImageElement> = new Map();

/** A require context used for importing image URLs. */
const imageRequireContext = require.context('../assets/', true, /\.(png|jpg)$/);

// Populate the map of image file paths to URLs.
imageRequireContext.keys().forEach((imageFilename: string) => {
  imageURLs.set(imageFilename, imageRequireContext(imageFilename));
});

/**
 * Get the URL associated with a given image filename.
 *
 * @param imageFilename - The filename of the image.
 * @returns The URL of the image.
 */
export function getImageURL(imageFilename: string): string {
  return imageURLs.get(imageFilename)!;
}

/**
 * Get the `HTMLImageElement` associated with a given image filename.
 *
 * @param imageFilename - The filename of the image.
 * @returns The image element.
 */
export function getImageElement(imageFilename: string): HTMLImageElement {
  return imageElements.get(imageFilename)!;
}

/**
 * Load a list of images and store them as `HTMLImageElement`s.
 * Call a callback function after all of the images have finished loading.
 *
 * @param imageFilenames - A set of image filenames.
 * @param callback - A function that is called when all of the images have finished loading.
 * @returns A promise that resolves to an array of void.
 */
export async function loadImages(imageFilenames: Set<string>): Promise<void[]> {
  return Promise.all(Array.from(imageFilenames).map(
    (imageFilename: string) => new Promise<void>((resolve) => {
      const imageElement: HTMLImageElement = new global.Image();
      imageElement.addEventListener('load', () => {
        imageElements.set(imageFilename, imageElement);
        resolve();
      });
      imageElement.src = getImageURL(imageFilename);
    }),
  ));
}
