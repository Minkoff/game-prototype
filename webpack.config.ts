const HtmlPlugin = require('html-webpack-plugin');
// eslint-disable-next-line import/no-extraneous-dependencies
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: './src/ts/Game.ts',
  resolve: {
    extensions: ['.ts', '.js'],
    modules: ['src/ts', 'node_modules'],
  },
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                ['@babel/preset-env', {
                  targets: {
                    browsers: ['last 2 chrome versions'],
                  },
                }],
                '@babel/preset-typescript',
              ],
              plugins: [
                '@babel/plugin-proposal-class-properties',
              ],
              sourceMaps: true,
            },
          },
          'eslint-loader',
        ],
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: true },
          },
        ],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader',
        ],
      },
    ],
  },
  plugins: [
    new HtmlPlugin({
      template: './src/html/index.html',
      filename: './index.html',
    }),
    new CopyPlugin({
      patterns: [
        { from: 'css/*.*', context: 'src/html' },
        { from: 'images/*.*', context: 'src/html' },
      ],
    }),
  ],
  mode: 'development',
};
